package info.hccis.performancehall_mobileappbasicactivity.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

/**
 * Class brought from the Performance Hall web application.  It also has some additional methods
 * related to Ticket Orders.  It's attributes correspond to the attributes which are found in
 * the database.
 *
 * @author bjmaclean
 * @since 20220202
 */
@Entity(tableName = "TicketOrder")
public class TicketOrder implements Serializable {

    public static final double MAX_TICKETS =100;
    public static final double COST_TICKET =10;
    public static final double DISCOUNT_HOLLPASS = 0.1;
    public static final double DISCOUNT_VOLUME_10 = 0.1;
    public static final double DISCOUNT_VOLUME_20 = 0.15;

    //Note these match the field names from the database (and keys in json)
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int numberOfTickets;
    private double costOfTickets;
    private int hollpassNumber;


    public TicketOrder() {
    }

    public TicketOrder(Integer id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumberOfTickets() {
        return numberOfTickets;
    }

    public void setNumberOfTickets(int numberOfTickets) {
        this.numberOfTickets = numberOfTickets;
    }

    public double getCostOfTickets() {
        return costOfTickets;
    }

    public void setCostOfTickets(double costOfTickets) {
        this.costOfTickets = costOfTickets;
    }

    public int getHollpassNumber() {
        return hollpassNumber;
    }

    public void setHollpassNumber(int hollpassNumber) {
        this.hollpassNumber = hollpassNumber;
    }

    /**
     * Calculate the cost for a ticket order
     *
     * @return cost
     * @since 20220105
     * @author cis2250
     */
    public double calculateTicketPrice() {
        double discount = 0;
        if(numberOfTickets >=20){
            discount += DISCOUNT_VOLUME_20;
        }else if(numberOfTickets >=10){
            discount += DISCOUNT_VOLUME_10;
        }

        if(validateHollPassNumber()){
            discount += DISCOUNT_HOLLPASS;
        }

        costOfTickets = numberOfTickets * COST_TICKET * (1-discount);

        return costOfTickets;
    }

    /**
     * Verify if a HollPassNumber if correct
     * Rules:  Length is 5 and a multiple of 13
     *
     * @return valid result
     * @since 20220105
     * @author cis2250
     */
    public boolean validateHollPassNumber() {

        boolean valid = false;

        if(hollpassNumber >= 10000 && hollpassNumber < 100000){
            if(hollpassNumber % 13 == 0){
                valid=true;
            }
        }

        return valid;
    }

    /**
     * Return if a valid number of tickets or not
     * @return true if valid
     * @since 20220117
     * @author BJM
     */
    public boolean validateNumberOfTickets(){
        if(numberOfTickets > 0 && numberOfTickets <= MAX_TICKETS){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public String toString() {
        return "Ticket Order" + System.lineSeparator()
                + "HollPass Number=" + hollpassNumber +System.lineSeparator()
                + "Number of Tickets=" + numberOfTickets + System.lineSeparator()
                + "Order cost: $" + calculateTicketPrice();
    }
}
